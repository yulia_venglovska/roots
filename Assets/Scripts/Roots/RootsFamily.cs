using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootsFamily : MonoBehaviour
{
    [SerializeField]
    private GameObject closeButton;
    [SerializeField]
    private GameObject exitButton;
    [SerializeField]
    private GameObject textWin;

    public GameObject sister;
    public GameObject brother;
    public GameObject mother;
    public GameObject father;
    public GameObject grandmother;
    public GameObject grandfather;

    public string newFoxToShow;

    public void ShowPortrait()
    {
        if (newFoxToShow == "sister")
        {
            sister.SetActive(true);
        }
        else if (newFoxToShow == "brother")
        {
            brother.SetActive(true);
        }
        else if (newFoxToShow == "mother")
        {
            mother.SetActive(true);
        }
        else if (newFoxToShow == "father")
        {
            father.SetActive(true);
        }
        else if (newFoxToShow == "grandmother")
        {
            grandmother.SetActive(true);
        }
        else if (newFoxToShow == "grandfather")
        {
            grandfather.SetActive(true);
        }

        CheckWin();
    }

    private void CheckWin()
    {
        if (Manager.instance.foundFoxes.Contains("mother") && Manager.instance.foundFoxes.Contains("father")
            && Manager.instance.foundFoxes.Contains("brother") && Manager.instance.foundFoxes.Contains("sister")
            && Manager.instance.foundFoxes.Contains("grandmother") && Manager.instance.foundFoxes.Contains("grandfather"))
        {
            Debug.Log("Win");
            textWin.SetActive(true);
            exitButton.SetActive(true);
        }
        else
        {
            closeButton.SetActive(true);
        }
    }

    public void GameOver()
    {
        sister.SetActive(false);
        brother.SetActive(false);
        mother.SetActive(false);
        father.SetActive(false);
        grandmother.SetActive(false);
        grandfather.SetActive(false);
    }
}
