using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private Animator anim;
   
    
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

   

    public void FadeToLevel()
    {
        anim.SetTrigger("fade");
    }
     public void OnFadeComplete()
    {
       StartNewGame();
    }
     public void StartNewGame()
    {
       
        Debug.Log("Start new game");
        SceneManager.LoadScene(1);
    }

}
