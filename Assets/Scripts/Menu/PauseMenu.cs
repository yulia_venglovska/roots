using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    public GameObject objects;
    private Animator anim;
    public GameObject button;    
    void Start()
    {
         anim = GetComponent<Animator>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Debug.Log("Resume");
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        GameIsPaused = false;
        pauseMenuUI.SetActive(false);
        objects.SetActive(true);
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        objects.SetActive(false);
        GameIsPaused = true;
    }

    public void FadeToLevel()
    {
        anim.SetTrigger("fade");
    }
     public void OnFadeComplete()
    {
       Load();
    }
     public void Load()
    {
       
        Debug.Log("Load");
        Resume();
        if(button.activeInHierarchy)
        {
            SceneManager.LoadScene(3);
        }else
        {
            SceneManager.LoadScene(0);
        }
    }
   
    
    
}
