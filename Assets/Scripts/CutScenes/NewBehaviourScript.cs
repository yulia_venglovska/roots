using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class NewBehaviourScript : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        videoPlayer.Play();

    }
   void OnEnable() //Сначала подписываем нашу функцию на событие конца видео
    {
        videoPlayer.loopPointReached += OnVideoEnd;
    }

    void OnDisable() //Отписываем для предотвращения утечки памяти
    {
        videoPlayer.loopPointReached -= OnVideoEnd;
    }

    void Update()
    {
         if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("Кнопка нажата");
            FadeToLevel();
        }
      
    }
    void OnVideoEnd(UnityEngine.Video.VideoPlayer causedVideoPlayer)
    {
        FadeToLevel();
    }
      public void FadeToLevel()
    {
        anim.SetTrigger("fade");
    }
     public void OnFadeComplete()
    {
        Application.LoadLevel(2);
    }
}
