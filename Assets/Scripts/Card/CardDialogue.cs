using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class CardDialogue : MonoBehaviour
{
    
    public int nextNode;
    public GameObject frontSide;
    public SpriteRenderer CardSprite;
    public GameObject backSide;
    public TMP_Text frontText;
    public TMP_Text backText;
    public bool isBackSide = false;

    public string foundFox = "";
    public bool gameOver = false;

    
 

    private void OnMouseDown()
    {
        Debug.Log("Card clicked!");
        AudioManager.instance.Play("Flip");
       
        

        if (!isBackSide)
        {
            isBackSide = true;
   
    
            
    

            CardDialogue[] cardDialogues = FindObjectsOfType<CardDialogue>();
            foreach (CardDialogue card in cardDialogues)
            {
                if (this != card)
                {
                    Destroy(card.gameObject);
                }
            }

            frontSide.SetActive(false);
            backSide.SetActive(true);
        }
        else
        {
            InstantiateDialogue.instance.currentNode = nextNode;

            if (gameOver)
            {
                Debug.Log("Game over");
                FindObjectOfType<RootsFamily>().GameOver();
                Manager.instance.foundFoxes.Clear();
            }

            if (foundFox.Length > 0)
            {
                Debug.Log("fox " + foundFox);
                Manager.instance.foundFoxes.Add(foundFox);
                Manager.instance.roots.SetActive(true);

                FindObjectOfType<RootsFamily>().newFoxToShow = foundFox;

                FindObjectOfType<RootsFamily>().Invoke("ShowPortrait", 1);
                Destroy(this.gameObject);
            }
            else
            {
                InstantiateDialogue.instance.Interact();
            }
        }
    }
  
}
