using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    #region Singletone

    public static Manager instance;
 
    private void Awake()
    {
        instance = this;
    }

    #endregion

    public List<string> foundFoxes = new List<string>();
    public GameObject roots;


    private void Start()
    {
        
        if (PlayerPrefs.HasKey("Volume"))
            AudioListener.volume = PlayerPrefs.GetFloat("Volume");
        InstantiateDialogue.instance.Interact();
    }


    public void ExtiToMenu()
    {
        SceneManager.LoadScene(3);
    }
}
