using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InstantiateDialogue : MonoBehaviour
{
    #region Singletone

    public static InstantiateDialogue instance;
    private void Awake()
    {
        instance = this;
    }

    #endregion

    private TextAsset textXml;
    public TextAsset textXml_english;
    public TextAsset textXml_ukrainian;

    public Dialogue dialogue;
    public int currentNode;

    public TMP_Text titleText;
    public GameObject cardsParent;
    public GameObject cardPrefab;

    [SerializeField] GameObject forest;
    [SerializeField] GameObject garden;
    [SerializeField] GameObject river;
    [SerializeField] GameObject house;
    [SerializeField] GameObject forestEdge;

    public void Interact()
    {
        string language = PlayerPrefs.GetString("Language");
        if (language == "Ukrainian")
            textXml = textXml_ukrainian;
        else
            textXml = textXml_english;

        dialogue = Dialogue.Load(textXml);
        ShowCards();
    }

    void ShowCards()
    {
        foreach (Transform child in cardsParent.transform)
        {
            Destroy(child.gameObject);
        }

        titleText.text = dialogue.nodes[currentNode].titletext;

        int lengthCards = 0;
        List<GameObject> addedCards = new List<GameObject>();

        for (int i = 0; i < dialogue.nodes[currentNode].cards.Length; i++)
        {
            if (dialogue.nodes[currentNode].cards[i].foxWasNotFound != null
                && Manager.instance.foundFoxes.Contains(dialogue.nodes[currentNode].cards[i].foxWasNotFound))
            {
                Debug.Log("Card was not added because this fox has been found");
            }
            else
            {
                GameObject card = Instantiate(cardPrefab, cardsParent.transform);
                addedCards.Add(card);

                if (lengthCards == 0)
                {
                    card.transform.localPosition = new Vector3(0, 0, 0);
                }
                else if (lengthCards == 1)
                {
                    addedCards[0].transform.localPosition = new Vector3(-3, 0, 0);
                    card.transform.localPosition = new Vector3(3, 0, 0);
                }
                else if (lengthCards == 2)
                {
                    addedCards[0].transform.localPosition = new Vector3(-5, 0, 0);
                    addedCards[1].transform.localPosition = new Vector3(0, 0, 0);
                    card.transform.localPosition = new Vector3(5, 0, 0);
                }
                else if (lengthCards == 3)
                {
                    addedCards[0].transform.localPosition = new Vector3(-6.6f, 0, 0);
                    addedCards[1].transform.localPosition = new Vector3(-2.2f, 0, 0);
                    addedCards[2].transform.localPosition = new Vector3(2.2f, 0, 0);
                    card.transform.localPosition = new Vector3(6.6f, 0, 0);
                }


                card.GetComponent<CardDialogue>().frontText.text =
                    dialogue.nodes[currentNode].cards[i].frontText;
                card.GetComponent<CardDialogue>().backText.text =
                    dialogue.nodes[currentNode].cards[i].backText;
                card.GetComponent<CardDialogue>().nextNode =
                    dialogue.nodes[currentNode].cards[i].nextNode;

                if (dialogue.nodes[currentNode].cards[i].death)
                {
                    card.GetComponent<CardDialogue>().gameOver =
                    dialogue.nodes[currentNode].cards[i].death;
                }

                if (dialogue.nodes[currentNode].background.Length > 0)
                {
                    string back = dialogue.nodes[currentNode].background;
                    Debug.Log(back);

                    forest.SetActive(false);
                    garden.SetActive(false);
                    river.SetActive(false);
                    house.SetActive(false);
                    forestEdge.SetActive(false);

                    if (back == "forest")
                    {
                        forest.SetActive(true);
                    }
                    else if (back == "garden")
                    {
                        garden.SetActive(true);
                    }
                    else if (back == "river")
                    {
                        river.SetActive(true);
                    }
                    else if (back == "house")
                    {
                        house.SetActive(true);
                    }
                    else if (back == "forestEdge")
                    {
                        forestEdge.SetActive(true);
                    }
                }

                if (dialogue.nodes[currentNode].cards[i].fox != null)
                {
                    card.GetComponent<CardDialogue>().foundFox =
                        dialogue.nodes[currentNode].cards[i].fox;
                }

                if (dialogue.nodes[currentNode].cards[i].textImage.Length > 0)
                {
                    Debug.Log("Sprite");
                    card.GetComponent<CardDialogue>().CardSprite.sprite = Resources.Load<Sprite>("SpritesForCards/" + dialogue.nodes[currentNode].cards[i].textImage);
                }

                lengthCards++;
            }
        }
    }
}
