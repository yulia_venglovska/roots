using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("dialogue")]
public class Dialogue
{
    [XmlElement("node")]
    public Node[] nodes;

    public static Dialogue Load(TextAsset _xml)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Dialogue));
        StringReader reader = new StringReader(_xml.text);
        Dialogue dial = serializer.Deserialize(reader) as Dialogue;
        return dial;
    }
}

[System.Serializable]
public class Node
{
    [XmlElement("titletext")]
    public string titletext;

    [XmlArray("cards")]
    [XmlArrayItem("card")]
    public Card[] cards;

    [XmlAttribute("background")]
    public string background;
}

[System.Serializable]
public class Card
{
    [XmlAttribute("tonode")]
    public int nextNode;

    [XmlAttribute("textimage")]
    public string textImage;

    [XmlAttribute("fox")]
    public string fox;

    [XmlAttribute("nofox")]
    public string foxWasNotFound;

    [XmlAttribute("death")]
    public bool death;

    [XmlElement("fronttext")]
    public string frontText;

    [XmlElement("backtext")]
    public string backText;
}