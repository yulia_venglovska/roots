using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    [SerializeField]
    public string key;

    private LocalizationManager localizationManager;
    private Text text;
    private TMP_Text textTMP;

    void Awake()
    {
        if (localizationManager == null)
        {
            localizationManager = GameObject.FindObjectOfType<LocalizationManager>().GetComponent<LocalizationManager>();
        }
        if (text == null)
        {
            if (GetComponent<Text>())
                text = GetComponent<Text>();
            if (GetComponent<TMP_Text>())
                textTMP = GetComponent<TMP_Text>();
        }
        localizationManager.OnLanguageChanged += UpdateText;
    }

    void Start()
    {
        UpdateText();
    }

    private void OnDestroy()
    {
        localizationManager.OnLanguageChanged -= UpdateText;
    }

    virtual public void UpdateText()
    {
        if (gameObject == null) return;

        if (localizationManager == null)
        {
            localizationManager = GameObject.FindGameObjectWithTag("LocalizationManager").GetComponent<LocalizationManager>();
        }
        if (text == null)
        {
            if (GetComponent<Text>())
                text = GetComponent<Text>();
            if (GetComponent<TMP_Text>())
                textTMP = GetComponent<TMP_Text>();
        }
        if (textTMP)
            textTMP.text = localizationManager.GetLocalizedValue(key);
        else if (text)
            text.text = localizationManager.GetLocalizedValue(key);
    }
}
