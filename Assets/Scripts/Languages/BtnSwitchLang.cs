using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnSwitchLang : MonoBehaviour
{
    [SerializeField]
    private LocalizationManager localizationManager;

    public void switchLang(string language)
    {
        localizationManager.CurrentLanguage = language;
        Debug.Log("Now language is " + localizationManager.CurrentLanguage);
    }
}
